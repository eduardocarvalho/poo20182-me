package atividade07.tui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import atividade07.business.VeiculoBO;
import atividade07.domain.Contrato;
import atividade07.domain.Veiculo;
import enums.TipoVeiculo;

public class ContratoTui {

	public static List<Veiculo> veiculoList = new ArrayList<>();
	public static List<Contrato> contratoList = new ArrayList<>();
	public static List<Veiculo> contratoVeiculoList = new ArrayList<>();

	public static Scanner sc = new Scanner(System.in);
	public static Integer tipo = 0, numeroContrato = 1, testNumero = 0, asd = 0;
	public static boolean x = true;

	public static void menuEscolha() {

		System.out.println("\t    [---MENU DE OP��ES---] \n \t [1] Contrato \n \t [2] Veiculo \n \t [3] Sair \n");
		Integer escolha = sc.nextInt();
		sc.nextLine();
		if (escolha == 1) {
			System.out.println(
					"\t    [---CONTRATO---] \n \t [1] Adicionar Contrato \n \t [2] Adicionar ve�culo ao Contrato \n \t [3] Remover Ve�culo do Contrato \n \t [4] Listar Contratos \n \t [5] Voltar para Menu Principal");
			escolha = sc.nextInt();
			sc.nextLine();
			if (escolha == 1) {
				adicionarContrato();
			} else if (escolha == 2) {
				adicionarVeiculoContrato();
			} else if (escolha == 3) {
				removerVeiculoContrato();
			} else if (escolha == 4) {
				listarContrato();
			} else {

			}
		} else if (escolha == 2) {
			System.out.println(
					"\t    [---VE�CULOS---] \n \t [1] Adicionar Ve�culos \n \t [2] Consultar Lista de Ve�culos \n \t [3] Remover Ve�culo \n");
			escolha = sc.nextInt();
			sc.nextLine();
			if (escolha == 1) {
				adicionarVeiculo();
			} else if (escolha == 2) {
				listarVeiculo();
			} else if (escolha == 3) {
				System.out.println("\n Informe a placa do ve�culo que deseja remover: ");
				String placa = sc.nextLine();

				if (removerVeiculo(placa) == true) {
					System.out.println("Veiculo removido com sucesso! ");
					menuEscolha();
				} else {
					System.out.println("N�o h� ve�culo com esta placa");
					menuEscolha();
				}

			}

		}
	}

	public static void listarContrato() {
		Integer cont = 0;
		if (contratoList.size() == 0) {
			System.out.println("N�o h� contratos na lista!\n");
			menuEscolha();
		}

		System.out.println(contratoList.size() + " --> quantidade de contratos na lista.\n");

		for (Contrato contratos : contratoList) {
			List<Veiculo> veiculos = contratos.getVeiculosContrato();
			System.out.println("\n Contrato #" + (cont + 1));
			System.out.println("Nome: " + contratos.getNomeCliente());
			System.out.println("Endere�o: " + contratos.getEnderecoCliente());
			System.out.println("Valor do contrato: " + contratos.getValorContrato());
			System.out.println("\n \t VEICULO: ");

			if(contratos.getVeiculosContrato()==null) {
				System.out.println("N�o h� ve�culos relacionados ao contrato");
				menuEscolha();
			}
			for (Veiculo veiculo : veiculos) {
				System.out.println(" Placa: " + veiculo.getPlacaVeiculo() + "\n Ano: " + veiculo.getAnoFabricacao()
				+ "\n Valor: " + veiculo.getValorVeiculo() + " \n");

			}

			cont++;
		}

		menuEscolha();
	}

	public static void removerVeiculoContrato() {
		contratoVeiculoList = new ArrayList<>();
		Contrato contrato = new Contrato();

		System.out.println("Informe seu n�mero de contrato: ");
		Integer numeroContrato = sc.nextInt();
		sc.nextLine();

		for (Contrato contratos : contratoList) {
			if(numeroContrato == contratos.getNumeroContrato())
				contrato = contratos;
		}

		if(contrato.getVeiculosContrato()==null) {
			System.out.println("N�o existe ve�culos neste contrato");
			menuEscolha();
		}
		else {
			contratoVeiculoList = contrato.getVeiculosContrato();
			for (Veiculo veiculo : contratoVeiculoList) {
				System.out.println(" Placa: " + veiculo.getPlacaVeiculo() + "\n Ano: " + veiculo.getAnoFabricacao()
				+ "\n Valor: " + veiculo.getValorVeiculo() + " \n");
			}
			System.out.println("Informe a placa do ve�culo que deseja remover: ");
			String removerVeiculo = sc.nextLine();

			for (Veiculo veiculo : contratoVeiculoList) {
				if(removerVeiculo.equalsIgnoreCase(veiculo.getPlacaVeiculo())) {
					contratoVeiculoList.remove(veiculo);
					contrato.setVeiculosContrato( contratoVeiculoList);
					System.out.println("Veiculo removido com sucesso!");
					menuEscolha();
				}
			}System.out.println("N�o foram encontrados quaisquer veiculos que correspondem a placa: "+removerVeiculo);
			menuEscolha();

		}

	}

	public static void adicionarContrato() {
		Contrato contrato = new Contrato();
		System.out.println("O seu n�mero de contrato �: " + numeroContrato);
		contrato.setNumeroContrato(numeroContrato);

		System.out.println("Informe o nome: ");
		String nomeCliente = sc.nextLine();
		contrato.setNomeCliente(nomeCliente);

		System.out.println("Informe o endere�o: ");
		String enderecoCliente = sc.nextLine();
		contrato.setEnderecoCliente(enderecoCliente);
		numeroContrato++;
		contratoList.add(contrato);
		adicionarVeiculoContrato();
		menuEscolha();
	}

	public static void adicionarVeiculoContrato() {

		Contrato contrato = new Contrato();

		if (veiculoList.size() == 0) {
			System.out.println("N�o h� ve�culos dispon�veis.");
			menuEscolha();
		}

		Integer cont = 0;
		System.out.println("Informe seu n�mero de contrato: ");
		testNumero = sc.nextInt();
		sc.nextLine();
		System.out.println(contrato.getVeiculosContrato());



		if (testNumero > contratoList.size()) {
			System.out.println("N�mero inv�lido! ");
			adicionarVeiculoContrato();
		}

		for (Contrato contratos : contratoList) {
			if (contratos.getNumeroContrato() == testNumero)
				contrato = contratos;
			if(testNumero!=asd) {
				contratoVeiculoList = new ArrayList<>();
				asd = contrato.getNumeroContrato();
				if(contrato.getVeiculosContrato()!=null)
					contratoVeiculoList = contrato.getVeiculosContrato();
			}
		}
		System.out.println(contrato.getNomeCliente());
		System.out.println("Contrato Localizado! \n");


		System.out.println("Lista de ve�culos: ");
		for (Veiculo veiculos : veiculoList) {
			System.out.println("Veiculo #" + (cont));
			System.out.println("Placa: " + veiculos.getPlacaVeiculo());
			System.out.println("Tipo do ve�culo: " + veiculos.getTipoVeiculo());
			System.out.println("Ano de fabrica��o: " + veiculos.getAnoFabricacao());
			System.out.println(veiculos.getValorVeiculo() + " \n");
			cont++;
		}
		System.out.println("Selecione o n�mero correspondente do ve�culo que deseja adicionar ao contrato: ");
		Integer numeroVeiculo = sc.nextInt();
		cont = 0;

		for (Veiculo veiculos : veiculoList) {

			if (numeroVeiculo == cont) {


				contratoVeiculoList.add(veiculos);
				veiculoList.remove(veiculos);
				contrato.setValorContrato(veiculos.getValorVeiculo() + contrato.getValorContrato());
				contrato.setVeiculosContrato(contratoVeiculoList);

				System.out.println("Veiculo adicionado com sucesso na lista!");
				cont++;
				menuEscolha();
			}
			cont++;
		}
		menuEscolha();

	}

	public static void listarVeiculos() {
		Integer cont = 0;
		if (contratoList.size() == 0) {
			System.out.println("N�o h� ve�culos na lista!\n");
			menuEscolha();
		}

		System.out.println(veiculoList.size() + " --> quantidade de veiculos na lista.\n");

		for (Contrato contratos : contratoList) {
			System.out.println("Cliente #" + (cont + 1));
			System.out.println("Nome do cliente: " + contratos.getNomeCliente());
			System.out.println("Endere�o do cliente: " + contratos.getEnderecoCliente());
			System.out.println("Valor total do contrato: " + contratos.getValorContrato() + "\n");
			cont++;
		}

	}

	public static boolean removerVeiculo(String placa) {

		for (Veiculo veiculos : veiculoList) {
			if (placa.equals(veiculos.getPlacaVeiculo())) {
				veiculoList.remove(veiculos);
				return true;

			}
		}
		return false;
	}

	public static void adicionarVeiculo() {
		Veiculo veiculo = new Veiculo();

		do {
			System.out.println("Informe a placa: ");
			String placaVeiculo = sc.nextLine();
			veiculo.setPlacaVeiculo(placaVeiculo);
			if (VeiculoBO.validadorPlacaVeiculo(veiculo))
				System.out.println("J� existe um veiculo com esta placa!");
		} while (VeiculoBO.validadorPlacaVeiculo(veiculo));

		System.out.println("Informe o ano do ve�culo: ");
		Long anoFabricacao = sc.nextLong();
		veiculo.setAnoFabricacao(anoFabricacao);

		do {
			try {
				x = true;
				System.out.println("Informe o tipo do ve�culo [1]BASICO ~ [2]INTERMEDIARIO ~ [3]LUXO: ");
				tipo = sc.nextInt();

				x = false;
				if (tipo > 3 || tipo == 0) {
					System.out.println("Inv�lido, informe somente n�meros de 1 a 3.");
					x = true;
				}
			} catch (Exception e) {
				System.out.println("Informe somente 1, 2 ou 3.\n");
				sc.nextLine();
			}
		} while (x == true);

		switch (tipo) {
		case 1:
			veiculo.setTipoVeiculo(TipoVeiculo.BASICO);

			break;
		case 2:
			veiculo.setTipoVeiculo(TipoVeiculo.INTERMEDIARIO);

			break;

		case 3:
			veiculo.setTipoVeiculo(TipoVeiculo.LUXO);
			break;
		}

		if (veiculo.getTipoVeiculo() == TipoVeiculo.BASICO)
			veiculo.setValorVeiculo(100.45);

		else if (veiculo.getTipoVeiculo() == TipoVeiculo.INTERMEDIARIO)
			veiculo.setValorVeiculo(130.10);

		else
			veiculo.setValorVeiculo(156.00);

		veiculoList.add(veiculo);

		menuEscolha();

	}

	public static void listarVeiculo() {
		Integer cont = 0;
		if (veiculoList.size() == 0) {
			System.out.println("N�o h� ve�culos na lista!\n");
			menuEscolha();
		}

		System.out.println(veiculoList.size() + " --> quantidade de veiculos na lista.\n");

		for (Veiculo veiculos : veiculoList) {
			System.out.println("Veiculo #" + (cont + 1));
			System.out.println("Placa: " + veiculos.getPlacaVeiculo());
			System.out.println("Tipo do ve�culo: " + veiculos.getTipoVeiculo());
			System.out.println("Ano de fabrica��o: " + veiculos.getAnoFabricacao());
			System.out.println(veiculos.getValorVeiculo() + " \n");
			cont++;
		}

		menuEscolha();
	}

}
