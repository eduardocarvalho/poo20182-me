package atividade07.domain;

import java.util.ArrayList;
import java.util.List;

import atividade07.tui.ContratoTui;

public class Contrato {

	Integer numeroContrato;
	String nomeCliente;
	String enderecoCliente;
	List <Veiculo> veiculosContrato;
	
	
	double valorContrato;
	public void setVeiculosContrato(List<Veiculo> veiculosContrato) {
		this.veiculosContrato = veiculosContrato;
	}
	public Integer getNumeroContrato() {
		return numeroContrato;
	}
	public void setNumeroContrato(Integer numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getEnderecoCliente() {
		return enderecoCliente;
	}
	public void setEnderecoCliente(String enderecoCliente) {
		this.enderecoCliente = enderecoCliente;
	}	
	public double getValorContrato() {
		return valorContrato;
	}
	public List<Veiculo> getVeiculosContrato() {
		return veiculosContrato;
	}
	public void setValorContrato(double valorContrato) {
		this.valorContrato = valorContrato;
	}
	
	
}
