package atividade07.domain;

import enums.TipoVeiculo;

public class Veiculo {

	
	String placaVeiculo;
	long anoFabricacao;
	TipoVeiculo tipoVeiculo;
	double valorVeiculo;
	
	
	public double getValorVeiculo() {
		return valorVeiculo;
	}
	public void setValorVeiculo(double valorVeiculo) {
		this.valorVeiculo = valorVeiculo;
	}
	public TipoVeiculo getTipoVeiculo() {
		return tipoVeiculo;
	}
	public void setTipoVeiculo(TipoVeiculo tipoVeiculo) {
		this.tipoVeiculo = tipoVeiculo;
	}
	
	
	public String getPlacaVeiculo() {
		return placaVeiculo;
	}
	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}
	public long getAnoFabricacao() {
		return anoFabricacao;
	}
	public void setAnoFabricacao(long anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	
}
