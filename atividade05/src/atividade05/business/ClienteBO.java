package atividade05.business;

import atividade05.domain.Cliente;
import atividade05.persistance.ClienteDAO;


public class ClienteBO {

	ClienteBO clienteBO = new ClienteBO();
	
	public static String validarCliente(Cliente cliente) {
		
		if(validar(cliente) == null) {
			ClienteDAO.incluirCliente(cliente); 
			return null;
		}
		
		
		return validar(cliente);
		
	}

	private static String validar(Cliente cliente) {
		if(cliente.getNome().trim().isEmpty()){
			return "Nome do contato inv�lido.";
		}
		if(cliente.getTelefone().trim().isEmpty()){
			return "Telefone do contato inv�lido.";
		}
		if(cliente.getCpf().trim().isEmpty()){
			return "Cpf do contato inv�lido.";
		}
		if(cliente.getRg().trim().isEmpty()){
			return "RG do contato inv�lido.";
		}
	//	if(cliente.getCep().trim().isEmpty()){
			//return "CEP do contato inv�lido.";
		//}
		return null;
	}
}
