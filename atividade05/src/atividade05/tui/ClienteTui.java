package atividade05.tui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import atividade05.business.ClienteBO;
import atividade05.domain.Cliente;
import atividade05.enums.EstadoDaConta;
import atividade05.persistance.ClienteDAO;


public class ClienteTui {
	public static final String PASSWORD = "123";
	static Scanner sc = new Scanner(System.in);
	static Integer conta = 0, tentativa = 0;
	public static boolean x = false;


	public static void menuEscolha() {
		System.out.println("");
		System.out.println("\t ~ [0] TRANSA��ES ~ [1] INCLUIR ~ [2] LISTAR ~ [3] ADMINISTRADOR ~ [4]SAIR ~ ");
		int escolha = sc.nextInt();
		sc.nextLine();

		switch (escolha) {
		case 0:
			transacoes();
		case 1:
			cadastrarCliente();

			break;

		case 2:
			listarCliente();
			break;

		case 3:
			System.out.println("Digite a senha de admin:  ");
			String tentativaSenha = sc.nextLine();
			if(tentativa>2) {
				System.out.println("Voc� atingiu o limite de tentativas.");
				menuEscolha();
			}
			if(tentativaSenha.equals(PASSWORD) && tentativa<=2) {
				tentativa = 0;
				System.out.println("BEM VINDO ADM!\n");
				System.out.println("\t [1] BLOQUEAR ~ [2] DESBLOQUEAR ~ [3] ENCERRAR CLIENTE ~ [4] ADICIONAR SALDO ~ [5]SAIR");
				escolha = sc.nextInt();

				switch (escolha) {
				case 1:
					bloquearCliente();

					break;

				case 2:
					desbloquearCliente();
					break;

				case 3:
					encerrarCliente();
					break;

				case 4:
					adicionarSaldo();
					break;

				case 5:
					menuEscolha();
				}
			}else {

				System.out.println("Senha Inv�lida! ");

				tentativa++;
				menuEscolha();
			}

			break;
		}
	}


	public static void cadastrarCliente() {
		long rendaSalarial = 0;

		System.out.println("Informe o nome: ");
		String nome = sc.nextLine();


		System.out.println("Informe o telefone: ");
		String telefone = sc.nextLine();


		System.out.println("Informe o cpf: ");
		String cpf = sc.nextLine();


		System.out.println("Informe o cep: ");
		String cep = sc.nextLine();


		x = false;
		while(x == false) {
			try {
				System.out.println("Informe sua renda salarial: ");
				rendaSalarial = sc.nextLong();
				x = true;
			}
			catch(Exception e) {
				System.out.println("Informe somente n�meros inteiros.\n");
				sc.next();
			}
		}

		System.out.println("Informe o rg: ");
		String rg = sc.next();

		System.out.println("\n Conta ativa!");
		EstadoDaConta estado = EstadoDaConta.ATIVO;

		sc.nextLine();

		Double saldo = (double) 0;

		Cliente cliente = new Cliente(nome, telefone, cpf, cep, rendaSalarial, rg, conta, estado, saldo);
		conta++;
		if(ClienteBO.validarCliente(cliente) == null)
			System.out.println("Cliente adicionado na lista!");
		else {
			System.out.println(ClienteBO.validarCliente(cliente));
		}

		menuEscolha();
	}
	public static void transacoes() {
		List<Cliente> clientes = ClienteDAO.obterClientes();
		if(clientes.size()==0) {
			System.out.println("N�o h� clientes!");
			menuEscolha();
		}
		System.out.println("\t [1] CONSULTAR SALDO [2] DEP�SITO [3] SAQUE [4] MENU PRINCIPAL");
		int escolha = sc.nextInt();

		switch (escolha) {
		case 1:
			System.out.println("CONSULTAR SALDO");
			System.out.println("Informe sua conta: ");
			int informeConta = sc.nextInt();

			for (Cliente cliente : clientes) {
				if(informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVO) {
					System.out.println("Seu saldo �: "+cliente.getSaldo());

					menuEscolha();
				}
			}

			System.out.println("Conta inv�lida!");
			menuEscolha();



		case 2:
			System.out.println("DEP�SITO");
			System.out.println("Informe sua conta: ");
			informeConta = sc.nextInt();

			for (Cliente cliente : clientes) {
				if(informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVO) {
					System.out.println("Seu saldo �: "+cliente.getSaldo());
					System.out.println("Insira o valor de dep�sito: ");
					double depositarDinheiro = sc.nextDouble();
					cliente.setSaldo(depositarDinheiro + cliente.getSaldo());

					menuEscolha();
				}
			}

			System.out.println("Conta inv�lida!");
			menuEscolha();


		case 3:
			System.out.println("SACAR");
			System.out.println("Informe sua conta: ");
			informeConta = sc.nextInt();

			for (Cliente cliente : clientes) {
				if(informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVO) {
					System.out.println("Seu saldo �: "+cliente.getSaldo());
					System.out.println("Insira o valor que deseja sacar: ");
					double sacarDinheiro = sc.nextDouble();
					if(cliente.getSaldo()>=sacarDinheiro)
						cliente.setSaldo( cliente.getSaldo() - sacarDinheiro);
					else
						System.out.println("Voc� n�o possu� tanto dinheiro! ");
					menuEscolha();
				}

			}

			System.out.println("Conta inv�lida!");
			menuEscolha();


		}
	}
	public static void listarCliente() {

		List<Cliente> clientes = ClienteDAO.obterClientes();
		if(clientes.size()==0) {
			System.out.println("N�o h� contatos!");
			menuEscolha();
		}
		for(Cliente cliente : clientes) {
			if(cliente.getEstado()==EstadoDaConta.BLOQUEADO) 
				System.out.println("CLIENTE #"+cliente.getConta()+": "+cliente.getNome()+" est� BLOQUEADO!");

			else if(cliente.getEstado()==EstadoDaConta.ENCERRADO)
				System.out.println("A Conta #" + cliente.getConta()+" foi encerrada!" + cliente.getConta());
			else {
				System.out.println("\nCLIENTE #"+cliente.getConta());
				System.out.println();
				System.out.println("Nome do cliente: "+cliente.getNome());
				System.out.println("Telefone do cliente: "+cliente.getTelefone());
				System.out.println("CPF do cliente: "+cliente.getCpf());
				System.out.println("CEP do cliente: "+cliente.getCep());
				System.out.println("Renda salarial do cliente: "+cliente.getRendaSalarial());
				System.out.println("RG do cliente: "+cliente.getRg());
				System.out.println("Conta do cliente: "+cliente.getConta());
				System.out.println("Saldo da Conta: "+cliente.getSaldo());
				System.out.println("Estado da Conta: "+cliente.getEstado());

				System.out.println();
			}
		}menuEscolha();
	}
	public static void adicionarSaldo() {
		List<Cliente> clientes = ClienteDAO.obterClientes();
		if(clientes.size()==0) {
			System.out.println("N�o h� clientes!");
			menuEscolha();
		}
		System.out.println("Clientes e Saldos: ");
		for(Cliente cliente: clientes) {
			if(cliente.getEstado() == EstadoDaConta.ATIVO) {
				System.out.println("Nome: "+cliente.getNome()+" Saldo: "+cliente.getSaldo() +" Conta: "+cliente.getConta());
			}
		}
		System.out.println("Informe pela conta qual cliente deseja modificar o saldo: ");
		int escolhaConta = sc.nextInt();
		for (Cliente cliente2 : clientes) {
			if(cliente2.getConta() == escolhaConta && cliente2.getEstado() == EstadoDaConta.ATIVO) {
				System.out.println("Informe o saldo: ");
				cliente2.setSaldo(sc.nextDouble());
				menuEscolha();
			}
		}menuEscolha();

	}
	public static void bloquearCliente() {
		List<Cliente> clientes = ClienteDAO.obterClientes();
		if(clientes.size()==0) {
			System.out.println("N�o h� clientes!");
			menuEscolha();
		}

		System.out.println("Qual dos clientes deseja bloquear? ");

		for(Cliente cliente : clientes) {
			System.out.println("\nCLIENTE #"+cliente.getConta());
			System.out.println();
			System.out.println("\tNome do cliente: "+cliente.getNome() + "\tNumero da conta do cliente: " +cliente.getConta());
		}
		System.out.println("Informe o n�mero da conta referente ao cliente que desej�s bloquear: ");
		int escolhaBloquear = sc.nextInt();


		for(Cliente cliente : clientes) {
			if(escolhaBloquear == cliente.getConta()) {
				cliente.setEstado(EstadoDaConta.BLOQUEADO);
				System.out.println("O cliente "+cliente.getNome()+" foi bloqueado!");	 
				menuEscolha();
			}
			else {
				System.out.println("N�o h� cliente com n�mero da conta correspondente que possa ser bloqueado.");
				menuEscolha();
			}
		}

	}
	public static void desbloquearCliente() {
		List<Cliente> clientes = ClienteDAO.obterClientes();
		if(clientes.size()==0) {
			System.out.println("N�o h� clientes!");
			menuEscolha();
		}

		System.out.println("Qual dos clientes deseja desbloquear? ");

		for(Cliente cliente : clientes) {
			if(cliente.getEstado()==EstadoDaConta.BLOQUEADO) {
				System.out.println("\nCLIENTE #"+cliente.getConta());
				System.out.println();
				System.out.println("\tNome do cliente: "+cliente.getNome() + "\tNumero da conta do cliente: " +cliente.getConta());
				System.out.println("EST� BLOQUEADO!");
			}else {
				System.out.println("\nCLIENTE #"+cliente.getConta());
				System.out.println("\tNome do cliente: "+cliente.getNome() + "\tNumero da conta do cliente: " +cliente.getConta());
				System.out.println("EST� ATIVO!");
			}
		}

		System.out.println("Informe o n�mero da conta referente ao cliente que desej�s desbloquear: ");
		int escolhaDesbloquear = sc.nextInt();

		for(Cliente cliente : clientes) {
			if(escolhaDesbloquear == cliente.getConta() && cliente.getEstado()==EstadoDaConta.BLOQUEADO) {
				cliente.setEstado(EstadoDaConta.ATIVO);
				System.out.println("O cliente "+cliente.getNome()+" foi desbloqueado!");	 
				menuEscolha();
			}
			else {
				System.out.println("N�o h� cliente com n�mero da conta correspondente que possa ser bloqueado.");
				menuEscolha();
			}
		}
	}
	public static void encerrarCliente() {
		List<Cliente> clientes = ClienteDAO.obterClientes();
		if(clientes.size()==0) {
			System.out.println("N�o h� clientes!");
			menuEscolha();
		}

		System.out.println("Qual dos clientes deseja encerrar? ");

		for(Cliente cliente : clientes) {
			if(cliente.getEstado()==EstadoDaConta.BLOQUEADO) {
				System.out.println("\nCLIENTE #"+cliente.getConta());
				System.out.println();
				System.out.println("\tNome do cliente: "+cliente.getNome() + "\tNumero da conta do cliente: " +cliente.getConta());
				System.out.println("EST� BLOQUEADO!");
			}else if (cliente.getEstado()==EstadoDaConta.ATIVO){
				System.out.println("\nCLIENTE #"+cliente.getConta());
				System.out.println("\tNome do cliente: "+cliente.getNome() + "\tNumero da conta do cliente: " +cliente.getConta());
				System.out.println("EST� ATIVO!");
			}
		}

		System.out.println("Informe o n�mero da conta referente ao cliente que desej�s encerrar: ");
		int escolhaDesbloquear = sc.nextInt();

		for(Cliente cliente : clientes) {
			if(escolhaDesbloquear == cliente.getConta() && cliente.getEstado()==EstadoDaConta.ATIVO || cliente.getEstado()==EstadoDaConta.BLOQUEADO && cliente.getSaldo()<1) {
				cliente.setEstado(EstadoDaConta.ENCERRADO);
				System.out.println("O cliente "+cliente.getNome()+" foi encerrado!");	 
				menuEscolha();
			}else if (cliente.getSaldo()>1) {
				System.out.println("S� � poss�vel encerrar contas sem saldo, favor realize o saque dos "+cliente.getSaldo()+ "R$ restantes!");
				menuEscolha();
			}
			else {
				System.out.println("N�o h� cliente com n�mero da conta correspondente que possa ser encerrado.");
				menuEscolha();
			}
		}
	}

}
