package atividade05.domain;

import atividade05.enums.EstadoDaConta;

public class Cliente {
	
	private String nome;
	private String cpf;
	private String telefone;
	private String cep;
	private long rendaSalarial;
	private String rg;
	private Integer conta;
	private EstadoDaConta estado;
	private Double saldo;
	
	public Double getSaldo() {
		return saldo;
	}
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public Cliente(String nome, String cpf, String telefone, String cep, long rendaSalarial, String rg, Integer conta, EstadoDaConta estado, Double saldo) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.cep = cep;
		this.rendaSalarial = rendaSalarial;
		this.rg = rg;
		this.conta = conta;
		this.estado = estado;
		this.saldo = saldo;
	}

	public EstadoDaConta getEstado() {
		return estado;
	}
	public void setEstado(EstadoDaConta estado) {
		this.estado = estado;
	}
	public Integer getConta() {
		return conta;
	}
	public void setConta(Integer conta) {
		this.conta = conta;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public long getRendaSalarial() {
		return rendaSalarial;
	}
	public void setRendaSalarial(long rendaSalarial) {
		this.rendaSalarial = rendaSalarial;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	

}
