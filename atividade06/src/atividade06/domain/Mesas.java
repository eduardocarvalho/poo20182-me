package atividade06.domain;

import java.util.List;

import atividade06.enums.EstadoMesa;

public class Mesas {
	
Integer numeroMesa;
Integer capacidadeMesa;
EstadoMesa estadoMesa;
List<Pratos> pratosAtivos;
Double valorMesa;
Integer pessoasMesa;
public Integer getPessoasMesa() {
	return pessoasMesa;
}
public void setPessoasMesa(Integer pessoasMesa) {
	this.pessoasMesa = pessoasMesa;
}
public Double getValorMesa() {
	return valorMesa;
}
public void setValorMesa(Double valorMesa) {
	this.valorMesa = valorMesa;
}
public List<Pratos> getPratosAtivos() {
	return pratosAtivos;
}
public void setPratosAtivos(List<Pratos> pratosAtivos) {
	this.pratosAtivos = pratosAtivos;
}
public Integer getNumeroMesa() {
	return numeroMesa;
}
public void setNumeroMesa(Integer numeroMesa) {
	this.numeroMesa = numeroMesa;
}
public Integer getCapacidadeMesa() {
	return capacidadeMesa;
}
public void setCapacidadeMesa(Integer capacidadeMesa) {
	this.capacidadeMesa = capacidadeMesa;
}
public EstadoMesa getEstadoMesa() {
	return estadoMesa;
}
public void setEstadoMesa(EstadoMesa estadoMesa) {
	this.estadoMesa = estadoMesa;
}

}
