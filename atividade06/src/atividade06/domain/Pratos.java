package atividade06.domain;

import atividade06.enums.TipoPrato;

public class Pratos {

	public String getNomePrato() {
		return nomePrato;
	}
	public void setNomePrato(String nomePrato) {
		this.nomePrato = nomePrato;
	}
	public String getDescricaoPrato() {
		return descricaoPrato;
	}
	public void setDescricaoPrato(String descricaoPrato) {
		this.descricaoPrato = descricaoPrato;
	}
	public Double getValorPrato() {
		return valorPrato;
	}
	public void setValorPrato(Double valorPrato) {
		this.valorPrato = valorPrato;
	}
	public TipoPrato getTipoPrato() {
		return tipoPrato;
	}
	public void setTipoPrato(TipoPrato tipoPrato) {
		this.tipoPrato = tipoPrato;
	}
	
	public Integer getCodigoPrato() {
		return codigoPrato;
	}
	public void setCodigoPrato(Integer codigoPrato) {
		this.codigoPrato = codigoPrato;
	}

	Integer codigoPrato;
	String nomePrato;
	String descricaoPrato;
	Double valorPrato;
	TipoPrato tipoPrato;
	
}
