package atividade06.tui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import atividade06.domain.Mesas;
import atividade06.domain.Pratos;
import atividade06.enums.EstadoMesa;
import atividade06.enums.TipoPrato;

public class RestauranteTui {

	public static Scanner sc = new Scanner(System.in);
	public static List<Pratos> pratoLista = new ArrayList<>();
	public static List<Pratos> pratoListaAux = new ArrayList<>();
	public static List<Mesas> mesaLista = new ArrayList<>();
	public static Integer cont = 1, codigoMesa = 1;

	public static void menuPrincipal() {
		Integer escolhaMenu = 0;

		System.out.println("[1] Cliente - [2] Gerente - [3] Finalizar Sistema");
		escolhaMenu = sc.nextInt();

		switch (escolhaMenu) {

		case 1:
			System.out.println(
					"[1] Ver Menu de Pratos - [2] Selecionar Mesa - [3] Adicionar item  - [4] Pedir a conta - [5]Voltar");
			escolhaMenu = sc.nextInt();
			switch (escolhaMenu) {
			case 1:
				mostrarMenu();

				break;
			case 2:
				selecionarMesa();

			case 3:
				adicionarItem();
			case 4:
				pedirConta();
				break;
			default:
				menuPrincipal();
				break;

			}

			break;

		case 2:

			System.out.println(
					"[1] - Cadastrar Prato - [2] Cadastrar Mesa - [3] Mostrar Mesas - [4] Limpar Mesa - [5] Voltar");
			escolhaMenu = sc.nextInt();

			switch (escolhaMenu) {
			case 1:
				cadastrarPrato();

				break;
			case 2:
				cadastrarMesa();

			case 3:
				mostrarMesa();
			case 4:
				limparMesa();
			default:
				menuPrincipal();
				break;
			}

		default:
			System.out.println("Obrigado por utilizar nosso sistema!");
			System.exit(0);
			break;
		}

	}

	public static void pedirConta() {
		int cont = 0;
		System.out.println("MESAS EM USO: ");
		for (Mesas mesa : mesaLista) {

			if (mesa.getEstadoMesa().equals(EstadoMesa.OCUPADA)) {
				System.out.println("C�digo da mesa: " + mesa.getNumeroMesa()
						+ "\n ----------------------------------------------------------------");
				System.out.println("\t Capacidade da Mesa: " + mesa.getCapacidadeMesa());
				System.out.println("\t Estado da Mesa: " + mesa.getEstadoMesa()
						+ " \n ------------------------------------------------------------------");
				cont++;
			}

		}
		if (cont == 0) {
			System.out.println("N�o h� mesas ocupadas que possa pedir conta.");
			menuPrincipal();
		}

		System.out.println("Informe o n�mero da sua mesa: ");
		Integer numeroMesa = sc.nextInt();
		for (Mesas mesa : mesaLista) {

			if (numeroMesa == mesa.getNumeroMesa()) {
				System.out.println("Mesa localizada! ");
				System.out.println("Sua conta: " + mesa.getValorMesa() + "R$.");
				Double valorPessoa = mesa.getValorMesa() / mesa.getPessoasMesa();
				System.out.println("Se dividirmos por: " + mesa.getPessoasMesa() + " o valor para cada ser�: "
						+ mesa.getValorMesa());

				System.out.println("Obrigado por frequentar o Restaurante do Pov�o! ");
				mesa.setEstadoMesa(EstadoMesa.MANUTENCAO);
				menuPrincipal();

			}
		}

	}

	public static void limparMesa() {
		int cont = 0;

		for (Mesas mesa : mesaLista) {
			if (mesa.getEstadoMesa().equals(EstadoMesa.MANUTENCAO)) {
				System.out.println("C�digo da mesa: " + mesa.getNumeroMesa()
						+ "\n ----------------------------------------------------------------");
				System.out.println("\t Capacidade da Mesa: " + mesa.getCapacidadeMesa());
				System.out.println("\t Estado da Mesa: " + mesa.getEstadoMesa()

						+ " \n ------------------------------------------------------------------");
				cont++;
			}
		}
		if (cont == 0) {
			System.out.println("N�o h� mesas em Manuten��o!");
			menuPrincipal();
		}
		System.out.println("Informe o c�digo da mesa que deseja realizar manuten��o: ");
		int codigoManu = sc.nextInt();
		for (Mesas mesa : mesaLista) {
			if (mesa.getNumeroMesa() == codigoManu) {
				
				mesa.setValorMesa((double) 0);
				mesa.setPratosAtivos(null);
				mesa.setEstadoMesa(EstadoMesa.LIBERADA);
				System.out.println("A mesa " + mesa.getNumeroMesa() + " acabou de sofrer manuten��o.");
				menuPrincipal();

			}
		}
	}

	public static void adicionarItem() {
		pratoListaAux = new ArrayList<>();

		if (mesaLista.size() == 0) {
			System.out.println("N�o h� mesas.");
			menuPrincipal();
		}
		int contMesa = 0;
		for (Mesas mesa : mesaLista) {
			if (mesa.getEstadoMesa().equals(EstadoMesa.OCUPADA)) {
				System.out.println("C�digo da mesa: " + mesa.getNumeroMesa()
						+ "\n ----------------------------------------------------------------");
				System.out.println("\t Capacidade da Mesa: " + mesa.getCapacidadeMesa());
				System.out.println("\t Estado da Mesa: " + mesa.getEstadoMesa()

						+ " \n ------------------------------------------------------------------");
				cont++;
			}

		}
		if (cont == 0) {
			System.out.println("N�o h� mesas selecionadas!!!");
			menuPrincipal();
		}

		System.out.println("Informe o c�digo da mesa que deseja adicionar um item: ");
		Integer codigoMesa = sc.nextInt();

		for (Mesas mesa : mesaLista) {
			System.out.println(mesa.getNumeroMesa());
			if (codigoMesa == mesa.getNumeroMesa() && mesa.getEstadoMesa().equals(EstadoMesa.OCUPADA)) {
				if (mesa.getPratosAtivos() != null) {
					pratoListaAux = mesa.getPratosAtivos();
				}

				System.out.println("Mesa #" + mesa.getNumeroMesa() + " encontrada!");
				System.out.println("Informe o c�digo do item que desej�s adicionar: ");
				menuPratos();
				Integer codigoItem = sc.nextInt();
				for (Pratos prato : pratoLista) {
					if (codigoItem == prato.getCodigoPrato()) {
						pratoListaAux.add(prato);
						mesa.setPratosAtivos(pratoListaAux);

						System.out.println("O prato: " + prato.getNomePrato() + " de valor: " + prato.getValorPrato()
								+ " foi adicionado com sucesso a mesa: " + mesa.getNumeroMesa());
						if (mesa.getValorMesa() != null)
							mesa.setValorMesa(mesa.getValorMesa() + prato.getValorPrato());
						else {
							mesa.setValorMesa(prato.getValorPrato());
						}
						menuPrincipal();

					}
				}

			}
		}

	}

	public static void selecionarMesa() {
		if (mesaLista.size() == 0) {
			System.out.println("N�o h� mesas cadastradas!");
			menuPrincipal();
		}
		for (Mesas mesa : mesaLista) {

			if (mesa.getEstadoMesa().equals(EstadoMesa.LIBERADA)) {
				System.out.println("C�digo da mesa: " + mesa.getNumeroMesa()
						+ "\n ----------------------------------------------------------------");
				System.out.println("\t Capacidade da Mesa: " + mesa.getCapacidadeMesa());
				System.out.println("\t Estado da Mesa: " + mesa.getEstadoMesa()
						+ " \n ------------------------------------------------------------------");
			}
		}
		System.out.println("Insira o c�digo da mesa que deseja utilizar: ");
		Integer codigoMesa = sc.nextInt();
		System.out.println("Quantas pessoas ir� utilizar a mesa? ");
		Integer pessoasMesa = sc.nextInt();
		for (Mesas mesa : mesaLista) {
			if (codigoMesa == mesa.getNumeroMesa()) {
				if (mesa.getCapacidadeMesa() < pessoasMesa) {
					System.out.println("Esta mesa n�o cabe tanta gente!!!");
					menuPrincipal();
				}
				mesa.setPessoasMesa(pessoasMesa);

				System.out
						.println("Codigo da mesa que quero utilizar: " + codigoMesa + " Mesa: " + mesa.getNumeroMesa());
				System.out.println("Mesa " + mesa.getNumeroMesa() + " foi selecionada com sucesso.");
				mesa.setEstadoMesa(EstadoMesa.OCUPADA);
				menuPrincipal();
			}
		}
		System.out.println("N�o h� mesa que corresponde a este codigo " + codigoMesa + ". ");
		menuPrincipal();
	}

	public static void mostrarMesa() {
		for (Mesas mesa : mesaLista) {
			System.out.println("C�digo da mesa: " + mesa.getNumeroMesa()
					+ "\n ----------------------------------------------------------------");
			System.out.println("\t Capacidade da Mesa: " + mesa.getCapacidadeMesa());
			System.out.println("\t Estado da Mesa: " + mesa.getEstadoMesa()
					+ " \n ------------------------------------------------------------------");

		}
		menuPrincipal();

	}

	public static void mostrarMenu() {
		menuPratos();
		menuPrincipal();
	}

	private static void menuPratos() {
		for (Pratos prato : pratoLista) {
			System.out.println("C�digo do prato: " + prato.getCodigoPrato() + " -- " + prato.getTipoPrato()
					+ "\n ----------------------------------------------------------------");

			System.out.println("\t" + prato.getNomePrato());
			System.out.println("\t" + prato.getDescricaoPrato());
			System.out.println("\t" + prato.getValorPrato()
					+ "R$ \n ----------------------------------------------------------------");

		}
	}

	public static void cadastrarMesa() {
		Mesas mesa = new Mesas();
		System.out.println("Cadastrando a mesa #" + codigoMesa);
		System.out.println("Informe a capacidade da mesa: ");
		mesa.setCapacidadeMesa(sc.nextInt());

		mesa.setNumeroMesa(codigoMesa);
		mesa.setEstadoMesa(EstadoMesa.LIBERADA);
		mesaLista.add(mesa);
		codigoMesa++;
		System.out.println("Cadastro da mesa foi realizado com sucesso! \n");
		menuPrincipal();
	}

	public static void cadastrarPrato() {
		Pratos prato = new Pratos();
		Integer tipoPrato = 0;
		String nomePrato = "";
		String descricaoPrato = "";
		Double valorPrato = (double) 0;

		System.out.println("Informe o tipo do prato: \t \n [1] Comida --- [2]Bebida\n");
		tipoPrato = sc.nextInt();
		if (tipoPrato == 1) {
			prato.setTipoPrato(TipoPrato.COMIDA);

		} else if (tipoPrato == 2) {
			prato.setTipoPrato(TipoPrato.BEBIDA);
		} else {
			System.out.println("N�mero inv�lido!");
			menuPrincipal();
		}
		sc.nextLine();

		System.out.println("Informe o nome do prato: ");
		prato.setNomePrato(nomePrato = sc.nextLine());

		System.out.println("Informe a descri��o do prato: ");
		prato.setDescricaoPrato(descricaoPrato = sc.nextLine());

		System.out.println("Informe o pre�o do prato: ");
		prato.setValorPrato(valorPrato = sc.nextDouble());

		prato.setCodigoPrato(cont);
		System.out.println("Cadastro do Prato #" + cont + " foi realizado!");

		cont++;
		pratoLista.add(prato);
		menuPrincipal();
	}

}
