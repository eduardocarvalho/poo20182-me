package br.ucsal.bes20181.poo.avaliacao1.tui;

import br.ucsal.bes20181.poo.avaliacao1.TipoVeiculoEnum;
import br.ucsal.bes20181.poo.avaliacao1.domain.Contrato;
import br.ucsal.bes20181.poo.avaliacao1.domain.Veiculo;

public class ContratoTui {

	public static void main(String[] args) {
	
		Veiculo veiculo1 = new Veiculo("ABC1", 2010, TipoVeiculoEnum.BASICO);
		Veiculo veiculo2 = new Veiculo("DEF2", 2013, TipoVeiculoEnum.BASICO);
		Veiculo veiculo3 = new Veiculo("GHI3", 2015, TipoVeiculoEnum.BASICO);
		
		Contrato contrato1 = new Contrato("Eduardo","Rua X");
		Contrato contrato2 = new Contrato("Rodrigo","Rua Y");
		contrato1.adicionarVeiculo(veiculo1);
		contrato2.adicionarVeiculo(veiculo2);
		contrato1.adicionarVeiculo(veiculo3);
		
		System.out.println(contrato1.toString()+"\n"+contrato2.toString());
		contrato2.removerVeiculo("DEF2");
		System.out.println("\n");
		System.out.println(contrato1.toString()+"\n"+contrato2.toString());
		
		
//		
		
	
	}

}
