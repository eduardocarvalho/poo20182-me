package br.ucsal.bes20181.poo.avaliacao1.domain;

import br.ucsal.bes20181.poo.avaliacao1.TipoVeiculoEnum;

public class Veiculo {

	private String placa;

	private Integer anoFabricacao;

	private TipoVeiculoEnum tipo;

	public Veiculo(String placa, Integer anoFabricacao, TipoVeiculoEnum tipo) {

		this.placa = placa;
		this.anoFabricacao = anoFabricacao;
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Veiculo [placa=" + placa + ", anoFabricacao=" + anoFabricacao + ", tipo=" + tipo + "]";
	}

	public TipoVeiculoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoVeiculoEnum tipo) {
		this.tipo = tipo;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public String getPlaca() {
		return placa;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

}
