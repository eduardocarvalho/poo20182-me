package br.ucsal.bes20181.poo.avaliacao1.domain;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.bes20181.poo.avaliacao1.TipoVeiculoEnum;

public class Contrato {

	private static Integer contador = 0;

	private Integer numero;

	private String nomeCliente;

	private String enderecoCliente;

	private List<Veiculo> veiculos = new ArrayList<>();

	@Override
	public String toString() {
		return "Contrato [numero=" + numero + ", nomeCliente=" + nomeCliente + ", enderecoCliente=" + enderecoCliente
				+ ", veiculos=" + veiculos + ", valorTotal=" + valorTotal + "]";
	}

	private Double valorTotal = 0d;

	public Contrato(String nomeCliente, String enderecoCliente) {
		contador++;
		this.numero = contador;
		this.nomeCliente = nomeCliente;
		this.enderecoCliente = enderecoCliente;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public String getEnderecoCliente() {
		return enderecoCliente;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public Boolean adicionarVeiculo(Veiculo veiculo) {
		if (veiculoNaLista(veiculo) != null) {
			return false;
		}
		veiculos.add(veiculo);
		adicionarVeiculoContrato(veiculo.getTipo());
		return true;

	}

	public Boolean removerVeiculo(String placa) {

		for (Veiculo veiculo : veiculos) {
		
			if (veiculo.getPlaca() == placa) {
				System.out.println("Veiculo " + veiculo.getPlaca() + "foi removido com sucesso!");
				
		
	
				veiculos.remove(veiculo);
				
				return true;
			}
		}
		System.out.println("N�o h� veiculo com esta placa na lista!");
		return false;

	}

	public List<Veiculo> consultarVeiculos(String tipoVeiculo) {
		TipoVeiculoEnum tipoVeiculoEnum = TipoVeiculoEnum.valueOf(tipoVeiculo);
		List<Veiculo> veiculosSelecionados = new ArrayList<>();
		for (Veiculo veiculo : veiculosSelecionados) {
			if (veiculo.getTipo().equals(tipoVeiculoEnum.toString())) {
				veiculosSelecionados.add(veiculo);
			}
		}
		if (veiculosSelecionados.isEmpty()) {
			System.out.println("N�o h� veiculos");
			return null;
		}
		return veiculosSelecionados;

	}

	public void adicionarVeiculoContrato(TipoVeiculoEnum tipo) {

		valorTotal += TipoVeiculoEnum.obterValorDiaria(tipo);

	}

	public Veiculo veiculoNaLista(Veiculo veiculo) {
		for (Veiculo veiculoAtual : veiculos) {
			if (veiculoAtual.getPlaca().equalsIgnoreCase(veiculo.getPlaca())) {
				System.out.println("J� existe um veiculo com essa placa!");
				return veiculo;
			}
		}
		return null;
	}

}
