package br.ucsal.bes20181.poo.avaliacao1;

public enum TipoVeiculoEnum {
	BASICO, INTERMEDIARIO, LUXO;

	public static Double obterValorDiaria(TipoVeiculoEnum tipo) {

		switch (tipo) {
		case BASICO:
			return 100.45;

		case INTERMEDIARIO:
			return 130.10;

		case LUXO:
			return 156.00;

		default:
			System.out.println("Valor inv�lido!");
			
			return null;
			
		}
	}
}
