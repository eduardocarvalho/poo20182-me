package br.ucsal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

public class Lista04 {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		int excluirContato =0; String pesquisaNome = "";
		int incluir = 0;
		metodoMenu();
		int opcao = sc.nextInt();
		int count = 0;
		if (opcao == 1 && incluir < 1) {
			System.out.println("Quantos contatos deseja incluir?");
			incluir = sc.nextInt();

		}
		int count1 = 0;
		Contato[] contato = new Contato[incluir];
		count++;
		while (opcao != 5) {
			if (count != 1) {
				metodoMenu();
				opcao = sc.nextInt();
			}


			if(contato==null){
				contato = new Contato[incluir];
			}


			switch (opcao) {
			case 1:
				if (count1 == 0) {
					for (int i = 0; i < incluir; i++) {
						Contato contatoOBJ = incluaContato();
						count++;
						contato[i] = contatoOBJ;
						count1++;	

					}
				} else {
					System.out.println("Contatos ja inclusos");
				}
				break;


			case 2: System.out.println("### LISTA DE CONTATOS ###\n\n");

			if(contato.length < 1) {
				System.out.println("Ainda n�o possui contatos!");
				break;
			}else {
				escrevaContato(contato);
				break;
			}

			case 3: System.out.println("\n ### EXCLUIR CONTATOS ###\n\n");


			for (int i = 0; i < contato.length; i++) {
				System.out.println(i+" para: "+contato[i].getNome());
			}

			System.out.println("Informe pelo n�mero que refer�ncia qual contato deseja excluir: ");
			excluirContato = sc.nextInt();

			contato[excluirContato] = null;

			System.out.println("\n Contato exclu�do com sucesso!");

			case 4: System.out.println("\n ### PESQUISA POR NOME ###\n\n");
			
			System.out.println("Informe o nome: ");
			pesquisaNome = sc.next();
			
			for (int i = 0; i < contato.length; i++) {
				if(contato[i].getNome().equalsIgnoreCase(pesquisaNome)) {
					if (contato[i]==null) continue;
					System.out.println(" \n"
							+ "\nNome do Contato "+(i+1)+": "+contato[i].getNome()
							+ "\nTelefone do Contato "+(i+1)+": "+contato[i].getTelefone()	
							+ "\nAno de nascimento do Contato "+(i+1)+": "+contato[i].getAnoNascimento()	
							+ "\nData de nascimento"+(i+1)+": "+contato[i].getDataNascimento());	
					break;
					
				}else {
					System.out.println("N�o foram encontrados quaisquer contatos com o nome: "+ pesquisaNome);
					break;
				}
			}
			
			
			default:
				break;
				
				
			}

		}
	}

	private static void metodoMenu() {
		System.out.println(
				"O que deseja fazer? \n [1] Incluir \n [2] Listar \n [3] Excluir \n [4] Pesquisar por nome \n [5]Sair do Programa");
	}

	private static Contato incluaContato() {
		String date = "";
		Contato contatoOBJ = new Contato();

		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		System.out.println("Insira o nome do contato: ");
		String nome = sc.next();
		contatoOBJ.setNome(nome);

		System.out.println("Insira o Telefone do Contato: ");
		String telefone = sc.next();
		contatoOBJ.setTelefone(telefone);

		System.out.println("Insira o ano de Nascimento do Contato:");
		Integer anoNascimento = sc.nextInt();
		contatoOBJ.setAnoNascimento(anoNascimento);

		System.out.println("Insira a Data de Nascimento do Contato: [FORMATO: ''##/##/####'']");
		date = sc.next();
		df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			contatoOBJ.setDataNascimento(df.parse(date));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return contatoOBJ;
	}

	private static void escrevaContato(Contato[] contato) {

		for (int i = 0; i < contato.length; i++) {
			if (contato[i]==null) continue;
			System.out.println(" \n"
					+ "\nNome do Contato "+(i+1)+": "+contato[i].getNome()
					+ "\nTelefone do Contato "+(i+1)+": "+contato[i].getTelefone()	
					+ "\nAno de nascimento do Contato "+(i+1)+": "+contato[i].getAnoNascimento()	
					+ "\nData de nascimento"+(i+1)+": "+contato[i].getDataNascimento());	

		}System.out.println("\n");
	}


}
