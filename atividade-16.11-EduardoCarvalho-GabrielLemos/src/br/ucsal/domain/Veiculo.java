package br.ucsal.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Veiculo implements Comparable<Veiculo> {

	private String placa;
	private Integer anoFabricacao;
	private Double valor;
	public static List<Veiculo> veiculoList = new ArrayList<>();

	public void inserir(String placa, Integer anoFabricacao, Double valor) {
		Veiculo veiculo = new Veiculo();
		veiculo.placa = placa;
		veiculo.anoFabricacao = anoFabricacao;
		veiculo.valor = valor;
		
		veiculoList.add(veiculo);
	}

	public void listar() {
		Collections.sort(veiculoList);
		for (Veiculo veiculo : veiculoList) {
			System.out.printf("Ve�culo: %s, %d, R$ %.2f", veiculo.placa, veiculo.anoFabricacao, veiculo.valor);
		}
	}

	@Override
	public int compareTo(Veiculo o) {
		return this.placa.compareTo(o.placa);
	}

}
