package br.ucsal.domain;

public class Veiculo implements Comparable<Veiculo> {

	private String placa;
	private String modelo;
	private Integer anoFabricacao;
	private Double valor;
	private Integer custoLocacao;

	@Override
	public String toString() {
		return "" + "placa=" + placa + ", modelo=" + modelo + ", anoFabricacao=" + anoFabricacao + ", valor= R$ " + valor
				+ "";
	}

	public Veiculo(String placa, String modelo, Integer anoFabricacao, Double valor, Integer custoLocacao) {
		super();
		this.placa = placa;
		this.modelo = modelo;
		this.anoFabricacao = anoFabricacao;
		this.valor = valor;
		this.custoLocacao = custoLocacao;
	}
	
	
	public Integer getCustoLocacao() {
		return custoLocacao;
	}

	public String getPlaca() {
		return placa;
	}

	public String getModelo() {
		return modelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public Double getValor() {
		return valor;
	}

	@Override
	public int compareTo(Veiculo outro) {
		return this.getPlaca().compareTo(outro.getPlaca());
	}
	
}
