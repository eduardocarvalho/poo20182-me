package br.ucsal.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Locadora {
	private static List<Veiculo> veiculos = new ArrayList<Veiculo>();
	
	public static void cadastrarOnibus(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdPassageiros) throws ValorNaoValidoException {
		if(valor <= 0) {
			throw new ValorNaoValidoException();
		}
		
		veiculos.add(new Onibus(placa, modelo, anoFabricacao, valor, qtdPassageiros));
	}
	
	public static void cadastrarCaminhao(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdEixos, Integer capacidadeCarga) throws ValorNaoValidoException {
		if(valor <= 0) {
			throw new ValorNaoValidoException();
		}
		
		veiculos.add(new Caminhao(placa, modelo, anoFabricacao, valor, qtdEixos, capacidadeCarga));
	}

	public static void listarVeiculosOrdenadoPorPlaca() {
		Collections.sort(veiculos);
		
		for(Veiculo veiculo: veiculos) {
			System.out.println(veiculo.toString());
		}
	}
	
	public static void listarVeiculosOrdenadoPorValor() {
		ComparadorValor comparador = new ComparadorValor();
		Collections.sort(veiculos, comparador);
		
		for(Veiculo veiculo: veiculos) {
			System.out.printf("Ve�culo:\n\tPlaca: %s\n\tValor: %.2f\n\tCusto da Loca��o: %d\n\n", veiculo.getPlaca(), veiculo.getValor(), veiculo.getCustoLocacao());
		}
	}
	
	public static void listarModelos() {
		ComparadorModelo comparador = new ComparadorModelo();
		Collections.sort(veiculos, comparador);
		
		String modelo = "";
		
		System.out.println("Modelos:");
		for(Veiculo veiculo: veiculos) {
			if(!veiculo.getModelo().equals(modelo)) {
				System.out.println("\t"+veiculo.getModelo());
				modelo = veiculo.getModelo();
			}
		}
	}
	
}
