package br.ucsal.domain;

public class Onibus extends Veiculo{

	private Integer qtdPassageiros;
	
	
	public Onibus(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdPassageiros) {
		super(placa, modelo, anoFabricacao, valor, custoLocacao(qtdPassageiros, TipoVeiculo.ONIBUS));
		this.qtdPassageiros = qtdPassageiros;
	}
	
	
	public Integer getQtdPassageiros() {
		return qtdPassageiros;
	}

	private static Integer custoLocacao(Integer qtdPassageiros, TipoVeiculo tipoVeiculo) {
		return qtdPassageiros * tipoVeiculo.getMultiplicador();
	}

	@Override
	public String toString() {
		return "Onibus [qtdPassageiros=" + qtdPassageiros +  ", "+ super.toString() + "]";
	}

}
