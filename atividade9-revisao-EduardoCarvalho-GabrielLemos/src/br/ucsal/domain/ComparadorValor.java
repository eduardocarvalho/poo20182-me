package br.ucsal.domain;

import java.util.Comparator;

public class ComparadorValor implements Comparator<Veiculo> {

	@Override
	public int compare(Veiculo veiculo1, Veiculo veiculo2) {
		return veiculo1.getValor().compareTo(veiculo2.getValor());
	}

}
