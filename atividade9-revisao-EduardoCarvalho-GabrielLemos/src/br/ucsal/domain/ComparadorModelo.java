package br.ucsal.domain;

import java.util.Comparator;

public class ComparadorModelo implements Comparator<Veiculo> {

	@Override
	public int compare(Veiculo veiculo1, Veiculo veiculo2) {
		return veiculo1.getModelo().compareTo(veiculo2.getModelo());
	}

}
