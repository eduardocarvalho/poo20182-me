package br.ucsal.domain;

public class Caminhao extends Veiculo {
	
	private Integer qtdEixos;
	private Integer capacidadeCarga;

	public Caminhao(String placa, String modelo, Integer anoFabricacao, Double valor, Integer qtdEixos, Integer capacidadeCarga) {
		super(placa, modelo, anoFabricacao, valor, custoLocacao(capacidadeCarga, TipoVeiculo.CAMINHAO));
		this.qtdEixos = qtdEixos;
		this.capacidadeCarga = capacidadeCarga;
		
	}
	
	
	public Integer getQtdEixos() {
		return qtdEixos;
	}



	public Integer getCapacidadeCarga() {
		return capacidadeCarga;
	}


	private static Integer custoLocacao(Integer capacidadeCarga, TipoVeiculo tipoVeiculo) {
		
		return capacidadeCarga * tipoVeiculo.getMultiplicador();
	}

	@Override
	public String toString() {
		return "Caminhao [qtdEixos=" + qtdEixos + ", capacidadeCarga=" + capacidadeCarga + ", "+ super.toString() + "]";
	}
	
	
}
