package br.ucsal.domain;

public enum TipoVeiculo {

	ONIBUS(10), CAMINHAO(8);

	
	private Integer multiplicador;

	private TipoVeiculo(Integer multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Integer getMultiplicador() {
		return multiplicador;
	}

}
