package br.ucsal.atividade4.tui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import br.ucsal.atividade4.business.ContatoBO;
import br.ucsal.atividade4.domain.Contato;

public class ContatoTui {
	public static Integer i = 0;

	public static Contato [] contato = new Contato[100];
	public static Scanner sc = new Scanner(System.in);
	public static Integer menuOpcao() {
		try {
			Integer escolha = 0;

			while(escolha<1 || escolha>5) {
				System.out.println("\n[1]INCLUIR \t [2]LISTAR \t [3]EXCLUIR \t [4]PROCURAR \t [5]PARAR");
				escolha = sc.nextInt();
				if(escolha<1 || escolha>5)
					System.out.println("Digite somente n�meros inteiros de 1 a 5.");

			}
			return escolha;}
		catch (Exception e) {
			sc.next();
			System.out.println("Digite somente n�meros inteiros de 1 a 5.");
			return menuOpcao();
		}
	}

	public static void verificarMenu(Integer escolha) {

		if(escolha==1) {
			incluirContato();
		}
		if(escolha==2) {
			listarContato();
		}
		if(escolha==3) {
			excluirContato();
		}
		if(escolha==4) {
			procurarContato();
		}
		if(escolha==5) {
			System.out.println("Programa Finalizado");
			System.exit(0);
		}

	}
	public static void incluirContato() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		contatoSet(df);	
		Agenda.exec();
	}

	public static void listarContato() {
		if(ContatoBO.validarContato(contato)==true) {
			System.out.println("N�o h� contatos!");
			Agenda.exec();
		}

		for (int i = 0; i < contato.length; i++) {
			if (contato[i]==null) continue;
			escrevaContato(i);	

		}System.out.println("\n");

		Agenda.exec();
	}

	public static void excluirContato() {

		if(ContatoBO.validarContato(contato)==true) {
			System.out.println("N�o h� contatos!");
			Agenda.exec();
		}
		else {
			System.out.println("Deseja excluir que contato? Referencie com o n�mero correspondente.");
			for (int i = 0; i < contato.length; i++) {
				if(contato[i] == null) continue;
				System.out.println((i+1)+" para: "+contato[i].getNome());
				System.out.println("Caso desista de excluie, escreva '0'");
			}
			Integer escolhaExcluir = sc.nextInt();
			if(escolhaExcluir ==0) {
				Agenda.exec();
			}
			if(contato[escolhaExcluir-1] == null) {
				System.out.println("Contato inexistente!");
			}
			contato[escolhaExcluir-1] = null;
			System.out.println("Contato exclu�do com sucesso");
			Agenda.exec();
		}
	}
	public static void procurarContato() {
		if(ContatoBO.validarContato(contato)==true) {
			System.out.println("N�o h� contatos!");
			Agenda.exec();
		}
		System.out.println("Informe o nome do contato que deseja procurar: ");
		String nomeProcurar = sc.next();
		for (int i = 0; i < contato.length; i++) {
			if(nomeProcurar.equalsIgnoreCase(contato[i].getNome())) {
				escrevaContato(i);	
				Agenda.exec();
			}
			else {
				System.out.println("N�o foi encontrado quaisquer contatos com essa especifica��o");
				Agenda.exec();
			}
		}
	}
	private static void escrevaContato(int i) {
		System.out.println(" \n"
				+ "Tal contato foi encontrado: "
				+ "\n\tNome do Contato "+(i+1)+": "+contato[i].getNome()
				+ "\n\tTelefone do Contato "+(i+1)+": "+contato[i].getTelefone()	
				+ "\n\tAno de nascimento do Contato "+(i+1)+": "+contato[i].getAnoNascimento()	
				+ "\n\tData de nascimento do contato"+(i+1)+": "+contato[i].getDataNascimento());
	}


	private static void contatoSet(SimpleDateFormat df) {
		try {
			Contato contatoIncluir = new Contato();

		System.out.println("Informe o nome do contato "+(i+1)+": ");
		String nome = sc.next();
		contatoIncluir.setNome(nome);

		System.out.println("Informe o telefone do contato "+(i+1)+": ");
		String telefone = sc.next();
		contatoIncluir.setTelefone(telefone);

		System.out.println("Informe o ano de nascimento do contato "+(i+1)+": ");
		Integer anoNascimento = sc.nextInt();
		contatoIncluir.setAnoNascimento(anoNascimento);

		System.out.println("Informe a Data de Nascimento no formato [dd/MM/yyyy]: ");
		String dataString = sc.next();
		try {
			contatoIncluir.setDataNascimento(df.parse(dataString));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		contato[i] = contatoIncluir;
		i++;
	}catch(Exception e) {
	System.out.println("Formato inv�lido");
	sc.next();
	Agenda.exec();
	}}
	

}